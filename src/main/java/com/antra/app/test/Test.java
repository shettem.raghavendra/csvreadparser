package com.antra.app.test;

import java.util.List;
import java.util.Map;

import com.antra.app.entity.Item;
import com.antra.app.util.CsvUtil;

public class Test {

	private static final String FILE_PATH_AND_NAME = "C:\\Users\\Shettem Raghavendra\\Documents\\data.csv";;

	public static void main(String[] args) {
		CsvUtil util = new CsvUtil(FILE_PATH_AND_NAME);
		List<Item> items = util.readCsvData();
		Map<Integer,List<String>> errorsMap = util.validateItems(items);
		/*Iterator<Entry<Integer,List<String>>> iterator = errorsMap.entrySet().iterator();
		while (iterator.hasNext()) {
			Entry<Integer, List<String>> entry =  iterator.next();
			System.out.println("FOR ITEM #" + entry.getKey());
			entry.getValue().forEach(System.out::println);
		}*/
		util.writeCsvData(items, errorsMap);
		System.out.println("DONE");
	}
}
