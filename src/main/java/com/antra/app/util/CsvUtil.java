package com.antra.app.util;

import java.io.BufferedWriter;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.csv.CSVRecord;

import com.antra.app.entity.Item;

public class CsvUtil {

	private String file = null;

	public CsvUtil(String file) {
		this.file = file;
	}

	public List<Item> readCsvData() {
		List<Item> items = new ArrayList<>();
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy");
		try ( 
				Reader reader = Files.newBufferedReader(Paths.get(file));
				CSVParser csvParser = new CSVParser(reader, CSVFormat.DEFAULT.withFirstRecordAsHeader()
						.withIgnoreHeaderCase()
						.withTrim())
				)
		{
			for (CSVRecord csvRecord : csvParser) {
				items.add(
						new Item(
								Integer.valueOf(csvRecord.get(0)),
								csvRecord.get(1),
								csvRecord.get(2),
								csvRecord.get(3),
								Double.valueOf(csvRecord.get(4)),
								sdf.parse(csvRecord.get(5)),
								sdf.parse(csvRecord.get(6)),
								Integer.valueOf(csvRecord.get(7))
								)
						);	
			}
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		return items;
	}

	public Map<Integer,List<String>> validateItems(List<Item> items) {
		Map<Integer,List<String>> errorsMap = new LinkedHashMap<>();
		for(Item item : items) {
			List<String> errors = new ArrayList<String>();
			if(item.getItemId()<=0) 
				errors.add("ITEM ID CAN NOT BE -VE OR ZERO");
			if(item.getItemMfgDate().after(item.getItemExpDate()))
				errors.add("MFG DATE CAN NOT BE AFTER EXP DATE");
			if(item.getItemPrice()<=0)
				errors.add("PRICE CAN NOT BE -VE OR ZERO");
			if(item.getItemQty()<=0)
				errors.add("QTY CAN NOT BE -VE OR ZERO");
			if(!errors.isEmpty())
				errorsMap.put(item.getItemId(), errors);
		}

		return errorsMap;
	}

	public boolean writeCsvData(List<Item> items, Map<Integer,List<String>> errors) {
		if(errors.isEmpty()) {
			try (
					BufferedWriter writer = Files.newBufferedWriter(Paths.get("./success/newData.csv"));
					CSVPrinter csvPrinter = new CSVPrinter(writer, CSVFormat.DEFAULT
							.withHeader("ITEM_ID", "ITEM_NAME", "ITEM_DESC", "ITEM_TYPE","ITEM_PRICE","ITEM_MFG_DATE","ITEM_EXP_DATE", "ITEM_QTY")
							)
					)  
			{
				csvPrinter.printRecord(items);
			} catch (Exception e) {
				e.printStackTrace();
			}

		} else {
			try (
					BufferedWriter writer = Files.newBufferedWriter(Paths.get("C:/errors/failData.csv"));
					CSVPrinter csvPrinter = new CSVPrinter(writer, CSVFormat.DEFAULT
							.withHeader("ITEM_ID", "ITEM_NAME", "ITEM_DESC", "ITEM_TYPE","ITEM_PRICE","ITEM_MFG_DATE","ITEM_EXP_DATE", "ITEM_QTY","ERRORS")
							)
					)  
			{

				for(Item item : items) {
					csvPrinter.print(item.getItemId());
					csvPrinter.print(item.getItemName());
					csvPrinter.print(item.getItemDesc());
					csvPrinter.print(item.getItemType());
					csvPrinter.print(item.getItemPrice());
					csvPrinter.print(item.getItemMfgDate());
					csvPrinter.print(item.getItemExpDate());
					csvPrinter.print(item.getItemQty());
					if(errors.get(item.getItemId())!=null)
						csvPrinter.print(errors.get(item.getItemId()).toString());
					else 
						csvPrinter.print("NO ERRORS");
					csvPrinter.println();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return false;
	}

}
