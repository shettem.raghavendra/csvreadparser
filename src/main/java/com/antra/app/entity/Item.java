package com.antra.app.entity;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Item {

	private Integer itemId;
	private String itemName;
	private String itemDesc;
	private String itemType;
	private Double itemPrice;
	private Date itemMfgDate;
	private Date itemExpDate;
	private Integer itemQty;
	
	
}
